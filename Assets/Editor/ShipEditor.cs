﻿#if UNITY_EDITOR
using UnityEngine;
using UnityEditor;

[CustomEditor(typeof(Enemy))]
public class ShipEditor : Editor {


    void OnSceneGUI()
    {
    Enemy ship = target as Enemy;

    EditorGUI.BeginChangeCheck();
    if (ship.wayPoints.Length > 1)
    {
        ship.wayPoints[0] = ship.transform.position;
        if (EditorGUI.EndChangeCheck())
        {
            Undo.RecordObject(ship, "Move Point");
            EditorUtility.SetDirty(ship);

        }
        for (int index = 1; index <= ship.wayPoints.Length - 1; index++)
        {
            Handles.DrawLine(ship.wayPoints[index - 1], ship.wayPoints[index]);
            EditorGUI.BeginChangeCheck();
            ship.wayPoints[index] = Handles.DoPositionHandle(ship.wayPoints[index], Quaternion.identity);
            if (EditorGUI.EndChangeCheck())
            {
                Undo.RecordObject(ship, "Move Point");
                EditorUtility.SetDirty(ship);
            }
        }
    }
    }
}
#endif