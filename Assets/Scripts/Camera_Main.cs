﻿using UnityEngine;
using System.Collections;

public class Camera_Main : MonoBehaviour {
    public Transform target;
    public float distanceToPlayer = 10,speed=5;
	void Start () {
        transform.position = target.position + new Vector3(0, distanceToPlayer, 0);
	}
	
	void Update () {
        MoveCamera();
	}

    void MoveCamera() { 
    transform.position+=transform.TransformDirection(Vector3.up)*speed*Time.deltaTime;
    }
}
