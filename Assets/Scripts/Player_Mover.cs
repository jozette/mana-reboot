﻿using UnityEngine;
using System.Collections;
[RequireComponent(typeof(Rigidbody))]
public class Player_Mover : MonoBehaviour
{
    public float speed = 10f, rateOfFire = .5f;
    public int maxBullets = 10;
    public GameObject bulletPrefab;
    private Bullet[] bullets;
    private int currentBullet;
    private Vector3 direction, velocityChange;

    private float rateOfFireTimer;
    void Start()
    {
        bullets = new Bullet[maxBullets + 1];
        for (int index = 0; index <= maxBullets; index++)
        {
            GameObject tmp = Instantiate(bulletPrefab) as GameObject;
            bullets[index] = tmp.GetComponent<Bullet>();
        }

        rigidbody.freezeRotation = true;
        rigidbody.useGravity = false;

    }

    void Update()
    {
        Firing();
    }
    void FixedUpdate()
    {
        MovePlayer();
    }
    void Firing()
    {
        if (rateOfFireTimer >= rateOfFire)
        {
            if (Input.GetKey(KeyCode.Space))
            {
                bullets[currentBullet].ReFire(transform.position);
                currentBullet = (currentBullet > maxBullets - 1) ? 0 : ++currentBullet;
                rateOfFireTimer = 0;
            }
        }
        else
        {
            rateOfFireTimer += Time.deltaTime;
        }

    }
    void MovePlayer()
    {
        direction.Set(Input.GetAxisRaw("Horizontal"), 0, Input.GetAxisRaw("Vertical"));
        velocityChange = direction * speed - rigidbody.velocity;
        rigidbody.AddForce(velocityChange, ForceMode.VelocityChange);
    }

}
